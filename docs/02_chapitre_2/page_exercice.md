---
author: Votre nom
title: 💻 Page d'exercice
---

# 💻 Page d'exercice

Le module `pydodide-mkdocs` permet d'insérer des éditeurs `Python` interactifs dans une page.

Le fonctionnement est relativement simple et repose tout d'abord sur la création de plusieurs fichiers.

Le nom de chaque fichier débute par la même chaîne correspond au titre de l'exercice. Vous pouvez choisir le titre que vous voulez.

Tous ces fichiers sont placés dans un sous-dossier `scripts/` à côté du fichier `markdown` décrivant l'exercice.

!!! abstract "Les différents fichiers"

    === "Fichier `nom_exo.py`"

        Ce fichier contient le code à compléter qui sera affiché dans l'éditeur.

        Il peut se terminer par des tests. Par exemple :

        ```python
        def bonjour(prenom):
        ...


        # tests
        assert bonjour("Nicolas") == "Bonjour Nicolas"
        ```

    === "Fichier `nom_exo_test.py`"

        Ce fichier contient les tests secrets qui seront effectués pour tester l'exactitude du code saisi par l'utilisateur.

        On y reprend le ou les tests publics et on les complète.

        ```python
        # Tests
        assert bonjour("Nicolas") == "Bonjour Nicolas"

        # Autres tests
        assert bonjour("Riri") == "Bonjour Riri"
        assert bonjour("Fifi") == "Bonjour Fifi"
        assert bonjour("Loulou") == "Bonjour Loulou"
        ```

    === "Fichier `nom_exo_corr.py`"

        Ce fichier contient le corrigé de l'exercice. Sans rien en plus.

        ```python
        def bonjour(prenom):
            return "Bonjour " + prenom
        ```

    === "Fichier `nom_exo_REM.py`"

        Il s'agit d'un fichier de remarques qui sera affiché une fois les tests passés (ou pas !).

        ```markdown
        Ce texte est affiché une fois que l'utilisateur a saisi un code qui passe tous les tests 
        ou lorsqu'il a essayé plusieurs fois sans succès.

        On peut par exemple indiquer des aides ou fournir d'autres versions :

         ```python
         def bonjour(prenom):
             return f"Bonjour {prenom}"
         ```
        ```

Une fois ces fichiers créés et placés dans le sous-dossier `scripts/`, il suffit d'insérer le code {% raw %} `{{ IDE('scripts/nom_exo') }} {% endraw %}` au sein d'une admonition `question`.

=== "Markdown"

    ```title=""
    !!! question "Nom de l'exercice"
            
        Énoncé de l'exercice.

        Notez qu'ici le fichier contenant l'exercice s'appelle `bonjour.py`.

        {% raw %}{{ IDE('scripts/bonjour') }}{% endraw %} 
    ```

=== "Rendu"

    !!! question "Nom de l'exercice"
            
        Énoncé de l'exercice.

        Notez qu'ici le fichier contenant l'exercice s'appelle `bonjour.py`.

        {{ IDE('scripts/bonjour') }}

!!! tip "Aller plus loin"

    La [page de présentation du module](https://bouillotvincent.gitlab.io/pyodide-mkdocs/install_ide/#prise-en-main) donne plus d'informations sur les possibilités de `pyodide-mkdocs`.