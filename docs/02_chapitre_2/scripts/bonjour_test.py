# Tests
assert bonjour("Nicolas") == "Bonjour Nicolas"

# Autres tests
assert bonjour("Riri") == "Bonjour Riri"
assert bonjour("Fifi") == "Bonjour Fifi"
assert bonjour("Loulou") == "Bonjour Loulou"
